# TISS Color Grade

## Purpose
TISS is the managment software for the Vienna University of Technology where students and professors can manage lectures,
exams and basically anything else. For students grades are a little bit hidden, since they are inside a small tooltip.
With this program, the grade is represented more visible with a colored background.

## Usage
To use this in your browser, you will need an extension where userscripts can be ran (eg. `TamperMonkey` for Chrome and Firefox).

 - Add the `TamperMonkey` extension to your browser of choice
 - Copy (Windows: STRG+C) the whole `script.js` from this repository to your clipboard
 - Click on the `TamperMonkey` icon and select `Create a new script...`
 - Paste (Windows: STRG+V) the script into the large text box
 - Save the script (STRG+S) and you are ready to go

## Demo
![Demo picture](https://gitlab.com/wolkoman/userscript-tiss-color-grade/raw/master/demo.png)

## License
MIT License
