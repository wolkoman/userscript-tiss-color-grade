// ==UserScript==
// @name         TISS Color Grade
// @namespace    http://tampermonkey.net/
// @version      0.2
// @updateURL    https://gitlab.com/wolkoman/userscript-tiss-color-grade/raw/master/script.js
// @downloadURL  https://gitlab.com/wolkoman/userscript-tiss-color-grade/raw/master/script.js
// @description  TISS. Make your grades visible.
// @author       You
// @match        https://tiss.tuwien.ac.at/education/favorites.xhtml?*
// @grant        none
// ==/UserScript==

(function() {
    var grade = (x) => {
        if(x.match(/participation with suc/) ||x.match(/excellent/) ||x.match(/sehr gut/) || x.match(/mit Erfolg teil/)) return "darkgreen";
        else if(x.match(/good/) ||x.match(/gut/)) return "limegreen";
        else if(x.match(/satis/) ||x.match(/befr/)) return "greenyellow";
        else if(x.match(/gen/)) return "yellow";
        else if(x.match(/nicht gen/)) return "black";
        else return "none";
    }
    Array.from(document.querySelectorAll("img"))
        .filter(x => x.id.endsWith("certificateHint:icon"))
        .forEach(x => {
            let gradeId = x.id.match(/(.*):icon/)[1];
        console.log(gradeId);
            let tooltip = document.querySelector(".ui-tooltip[id^='"+ gradeId +"']");
        console.log(tooltip);
            let gradeName = tooltip.querySelector(".ui-tooltip-text").innerHTML;
        console.log(gradeName);
            x.parentElement.parentElement.style.background = grade(gradeName);
            //x.parentElement.parentElement.innerHTML = "";
        }
    )
})();
